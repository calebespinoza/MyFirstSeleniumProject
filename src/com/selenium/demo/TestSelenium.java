package com.selenium.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestSelenium {
	public static void main (String args[]) {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.sucursalelectronica.com/redir/showLogin.go");
		driver.findElement(By.xpath("//form[@name='loginForm']/div[@class='container']/div[@class='carrousel-container']/div[@class='carrousel']/a")).click();
		driver.get("http://www.w3schools.com");
		driver.findElement(By.xpath("//div[@class='w3-bar-block']//a[@class='w3-bar-item w3-button'][contains(text(),'Learn HTML')]")).click();
		driver.close();
		
		System.setProperty("webdriver.gecko.driver", "GeckoDriver/geckodriver");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://www.sucursalelectronica.com/redir/showLogin.go");
		driver.findElement(By.xpath("//div[@class='carrousel']/a")).click();
		driver.close();
	}
}
